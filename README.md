# To run this project locally

1. `git clone https://gitlab.com/adamhrvth/adam-horvath-demo.git`
2. `cd adam-horvath-demo`
3. `npm install`
4. `npm start`
5. Now the project should be running in your browser.
