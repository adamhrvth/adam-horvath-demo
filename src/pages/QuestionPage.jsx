import React, { useState, useEffect } from 'react'

import { Link } from "react-router-dom";

import SelectOne from '../components/SelectOne.jsx';
import SelectMore from '../components/SelectMore.jsx';
import FreeText from '../components/FreeText.jsx';

const QuestionPage = ({ questionDetails, submitted, submitInput, index }) => {
  
  const [input, setInput] = useState(submitted ? submitted : () => {
    const storedInput = localStorage.getItem(`storedInput-${index}`);
    return storedInput ? JSON.parse(storedInput) : null;
  });

  useEffect(() => {
    localStorage.setItem(`storedInput-${index}`, JSON.stringify(input));
  }, [input]);

  const handleNextClick = (e) => {
    if (!submitted) {
      e.preventDefault();
      alert("Please enter/select your answer!");
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    submitInput(input);
    setInput(null);
  }

  return (
    <div className = "poll">
      <form onSubmit={handleSubmit}>
        <h2>{questionDetails.question}</h2>

        {
          questionDetails.type === "SELECT_ONE" ?
            <SelectOne questionDetails = {questionDetails} input = {input} setInput = {setInput} />
            : 
            questionDetails.type === "SELECT_MORE" ? 
              <SelectMore questionDetails = {questionDetails} input = {input} setInput = {setInput} />
              : 
              <FreeText questionDetails = {questionDetails} input = {input} setInput = {setInput} />  
        }

        <button
          className = "submitbutton"
        >Submit</button>
      </form>

    <Link 
      to = {`/question-${index - 1}`}
    >    
      <button
        disabled = {index < 2}
      >
        Previous
      </button>
    </Link>
      
    <Link 
      to = {index === 3 ? "/summary" : `/question-${index + 1}`}
      onClick = {handleNextClick}
    >
      <button
      >
        {
          index === 3 ? 
          "Finalize" : 
          "Next"
        }
      </button>
    </Link>
    </div>
  )
}

export default QuestionPage