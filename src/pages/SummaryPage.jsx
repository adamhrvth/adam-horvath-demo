import React from 'react'

const SummaryPage = ({ questions, answers }) => {

  return (
    <div className = "summary">
      <h2>Thank you for participating, your answers were:</h2>
      <ul className = "resultlist">
        {
          questions.map((item, index) => {
            return (
              <li> {item.question} Your answer: {answers[index]}</li>
            )
          })
        }
      </ul>

    </div>
  )
}

export default SummaryPage