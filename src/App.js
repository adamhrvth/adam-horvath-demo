import './App.css';
import React, { useState, useEffect } from 'react';

import { Route, Routes } from 'react-router-dom';

import pollData from "./assets/poll.json"; 

import QuestionPage from './pages/QuestionPage';
import SummaryPage from './pages/SummaryPage';

import { useNavigate } from 'react-router-dom';

function App() {
  const [answerOne, setAnswerOne] = useState(() => {
    const storedAnswerOne = localStorage.getItem('answerOne');
    return storedAnswerOne ? JSON.parse(storedAnswerOne) : null;
  });

  const [selectMore, setSelectMore] = useState(() => {
    const storedSelectMore = localStorage.getItem('selectMore');
    return storedSelectMore ? JSON.parse(storedSelectMore) : null;
  });

  const [freeText, setFreeText] = useState(() => {
    const storedFreeText = localStorage.getItem('freeText');
    return storedFreeText ? JSON.parse(storedFreeText) : null;
  });

  useEffect(() => {
    localStorage.setItem('answerOne', JSON.stringify(answerOne));
  }, [answerOne]);

  useEffect(() => {
    localStorage.setItem('selectMore', JSON.stringify(selectMore));
  }, [selectMore]);

  useEffect(() => {
    localStorage.setItem('freeText', JSON.stringify(freeText));
  }, [freeText]);

  const navigate = useNavigate();

  useEffect(() => {
    navigate("/question-1");
  }, [])

  return (
    <div className="App">
      
      <Routes>
        <Route 
          path = "question-1"
          element = {<QuestionPage questionDetails = {pollData[0]} submitted = {answerOne} submitInput = {setAnswerOne} index = {1}/>}
        />

        <Route 
          path = "question-2"
          element = {<QuestionPage questionDetails = {pollData[1]} submitted = {selectMore} submitInput = {setSelectMore} index = {2}/>}
        />

        <Route 
          path = "question-3"
          element = {<QuestionPage questionDetails = {pollData[2]} submitted = {freeText} submitInput = {setFreeText} index = {3}/>}
        />

        <Route 
          path = "summary"
          element = {
            <SummaryPage 
              questions = {pollData} 
              answers = {[answerOne, selectMore, freeText]}
            />
          }
        />
      </Routes>

    </div>
  );
}

export default App;
