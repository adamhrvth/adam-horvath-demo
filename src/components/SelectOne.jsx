import React from 'react'

const SelectOne = ({ questionDetails, input, setInput }) => {

  return (
    <div className = "userInput">
      {
        questionDetails.answers.map(option => {
          return (
            <div key={option}>
              <input
                type = "radio"
                id = { option }
                name = "answer"
                value = { option }
                checked = {input === option}
                onChange = {e => setInput(e.target.value)}
              />
              <label htmlFor={option}>{option}</label>
            </div>
          )
        })
      }
    </div>
  );
}

export default SelectOne