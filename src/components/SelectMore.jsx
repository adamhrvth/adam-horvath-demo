import React from 'react'

const SelectMore = ({ questionDetails, input, setInput }) => {

  const handleChange = (e) => {
    const newValue = e.target.value;
    const newAnswer = [];

    if (input) {
      newAnswer.push(...input);
    }

    if (e.target.checked) {
      newAnswer.push(newValue);
    } else {
      const index = newAnswer.indexOf(newValue);
      newAnswer.splice(index, 1);
    }
    setInput(newAnswer);
  }

  return (
    <div className = "userInput">

      {questionDetails.answers.map((option) => (
        <div key={option}>

          <input
            type="checkbox"
            id={option}
            name={option}
            value={option}
            checked={input ? input.includes(option) : false}
            onChange={handleChange}
          />

          <label htmlFor={option}>{option}</label>

        </div>
      ))}

    </div>
  )
}

export default SelectMore