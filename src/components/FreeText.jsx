import React from 'react'

const FreeText = ({ questionDetails, input, setInput }) => {

  return (
    <div className = "userInput">

      <textarea
        rows="6"
        cols="50"
        value={input}
        onChange={e => setInput(e.target.value)}
      />

    </div>
  );
}

export default FreeText